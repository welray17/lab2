FROM python:3.10

ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app/

RUN chmod -R 755 /app

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "127.0.0.1:8080"]
